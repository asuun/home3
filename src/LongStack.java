import java.util.Iterator;
import java.util.LinkedList;

public class LongStack {

    private final LinkedList<Long> magasin;
    private int SP;

   public static void main (String[] argum) {
      // TODO!!! Your tests here!
       LongStack myobject=new LongStack();
        Object cloneMag=new Object();
        int rnd=8;
       //test if empty, must be true
       System.out.println("is it empty? "+myobject.stEmpty());
       //push random  no-s
           for(int y=0;y<rnd;y++){
               double rand=Math.random()*10;
               long x=(long)rand;
               myobject.push(x);
           };

       String toStrings=new String();
       toStrings=myobject.toString();
       System.out.println("to string: "+ toStrings);
       System.out.println("is it empty? "+myobject.stEmpty());
           // pop and read
       StringBuilder build=new StringBuilder();
       String equations=new String();

       System.out.println(myobject.magasin);
       System.out.println(" \t  x \t \t (x-1)");
       for(int y=0;y<rnd/2;y++){
           System.out.println("read \t"+myobject.tos() + "\t \t"+myobject.tos());
           build.append((int)myobject.tos()+" ");
           System.out.print("pop  \t"+myobject.pop());
           build.append((int)myobject.tos()+" ");
           System.out.print("\t \t"+ myobject.pop()+"\n");
       };
        // append random op-s
       build.append("+ + + + + + +");
       equations=build.toString();
       System.out.println("calc "+myobject.interpret(equations));

        // rand calculation
       String testCalc="1800 90 10 * /";
       System.out.println("Random calculation "+myobject.interpret(testCalc)+"/t");

   }
    //Constructor for a new stack: LongStack()
   LongStack() {
      // TODO!!! Your constructor here!
       this.magasin=new LinkedList<>();
        SP=-1;
   }

    LongStack(LinkedList<Long> lifo) {
        this.magasin = lifo;
    }

    //Copy of the stack: Object clone()
   @Override
   public Object clone() throws CloneNotSupportedException {

       return new LongStack((LinkedList<Long>) this.magasin.clone());
   }




   //Check whether the stack is empty: boolean stEmpty()
   public boolean stEmpty() {
      return this.magasin.isEmpty(); // TODO!!! Your code here!
   }

   //Adding an element to the stack: void push (long a)
   public void push (long a) {
      // TODO!!! Your code here!
       this.magasin.push(a);
   }

   //Removing an element from the stack: long pop()
   public long pop() {
        return this.magasin.pop();
   } // pop

    /*Arithmetic operation s ( + - * / ) between two topmost elements of the stack (result is left on top):
    void op (String s)
     */
   public void op (String s) {
       if (s.equals("+")) {
           this.magasin.push(this.magasin.pop() + this.magasin.pop());
       } else if (s.equals("-")) {
           long op1 = this.pop();
           long op2 = this.pop();
           this.magasin.push(op2 - op1);
       } else if (s.equals("*")) {
           this.magasin.push(this.magasin.pop() * this.magasin.pop());
       } else if (s.equals("/")) {
           long op1 = this.pop();
           long op2 = this.pop();
           this.magasin.push(op2 / op1);
       } else if(s.equals(" ") | s.equals("\t")) {
           return;
       } else {
           throw new RuntimeException("Invalid operation");
       }
   }

    //Reading the top without removing it: long tos()
    public long tos() {
       // TODO!!! Your code here!
       return this.magasin.getFirst();
   }

    //Check whether two stacks are equal: boolean equals (Object o)
   @Override
   public boolean equals (Object o) {
       // TODO!!! Your code here!
       return this.magasin.equals(((LongStack)o).magasin);
   }

    //Conversion of the stack to string (top last): String toString()
   @Override
   public String toString() {
       // TODO!!! Your code here!
       StringBuilder s = new StringBuilder();
       Iterator<Long> i = this.magasin.descendingIterator();
       while (i.hasNext()) {
           s.append(i.next());
           s.append(" ");
       }
       return s.toString();
   }


   public static long interpret (String pol) {
       // TODO!!! Your code here!
       LongStack ls = new LongStack();

       for(int i = 0; i < pol.length(); i++) {
           char c = pol.charAt(i);
           char nc = ' ';

           if (i+1 < pol.length()) {
               nc = pol.charAt(i + 1);

           }
           if ((c == '-' & nc >= '0' & nc <= '9')| (c >= '0' & c <= '9')) {
               StringBuilder buf = new StringBuilder();
               buf.append(c);

               i++;
               for (; i < pol.length(); i++) {

                   c = pol.charAt(i);
                   if (c >= '0' & c <= '9') {
                       buf.append(c);
                   } else {
                       break;
                   }
               }
               ls.push(Long.parseLong(buf.toString()));
           } else {
               ls.op("" + c);
           }
       }
       if (ls.magasin.size() == 1) {
           return ls.pop();
       } else {
           throw new RuntimeException("Unbalanced polish notation");
       }

   }


}



